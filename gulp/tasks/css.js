var gulp = require("gulp"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssvars = require("postcss-simple-vars"),
    nested = require("postcss-nested"),
    cssImport = require("postcss-import"),
    mixins = require("postcss-mixins");

gulp.task("css", function(){
    return gulp.src("./app/assets/styles/style.css")
        .pipe(postcss([cssImport, mixins, cssvars, nested, autoprefixer]))
//        .on('error', function(errorInfo){
//            //khudse agar error handling karna h toh idhar kar sakte h. Now it is not complusory pehle complusory tha and pehle gulp task ka watching band kar deta tha abh nai karta h.    
//        })
        .pipe(gulp.dest("./app/css"));
});